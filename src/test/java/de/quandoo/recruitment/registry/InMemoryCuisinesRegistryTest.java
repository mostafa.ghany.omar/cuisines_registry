package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.CuisineEnum;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class InMemoryCuisinesRegistryTest {

    private CuisinesRegistry cuisinesRegistry;
    String customer1Id = UUID.randomUUID().toString();
    String customer2Id = UUID.randomUUID().toString();
    String customer3Id = UUID.randomUUID().toString();

    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalErr = System.err;

    @After
    public void destroy() {
        System.setErr(originalErr);
    }

    @Before
    public void setUp(){
        cuisinesRegistry = new InMemoryCuisinesRegistry();

        final Cuisine frenchCuisine = new Cuisine(CuisineEnum.FRENCH);
        final Cuisine germanCuisine = new Cuisine(CuisineEnum.GERMAN);
        final Cuisine italianCuisine = new Cuisine(CuisineEnum.ITALIAN);

        cuisinesRegistry.register(new Customer(customer1Id), frenchCuisine);

        cuisinesRegistry.register(new Customer(customer2Id), frenchCuisine);
        cuisinesRegistry.register(new Customer(customer2Id), germanCuisine);

        cuisinesRegistry.register(new Customer(customer3Id), frenchCuisine);
        cuisinesRegistry.register(new Customer(customer3Id), germanCuisine);
        cuisinesRegistry.register(new Customer(customer3Id), italianCuisine);

        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void shouldGetCustomerCuisines() {
        Set<Cuisine> values = new HashSet<>();
        values.add(new Cuisine(CuisineEnum.FRENCH));
        values.add(new Cuisine(CuisineEnum.GERMAN));
        values.add(new Cuisine(CuisineEnum.ITALIAN));

        final List<Cuisine> customer3Cuisines = cuisinesRegistry.customerCuisines(new Customer(customer3Id));
        Assert.assertEquals(3, customer3Cuisines.size());
        Assert.assertTrue(values.containsAll(customer3Cuisines));

        final List<Cuisine> customer1Cuisines = cuisinesRegistry.customerCuisines(new Customer(customer1Id));
        Assert.assertEquals(1, customer1Cuisines.size());
        Assert.assertEquals(CuisineEnum.FRENCH, customer1Cuisines.get(0).getName());
    }


    @Test
    public void shouldGetCuisineCustomers() {
        final List<Customer> germanCuisineCustomers = cuisinesRegistry.cuisineCustomers(new Cuisine(CuisineEnum.GERMAN));
        final List<Customer> frenchCuisineCustomers = cuisinesRegistry.cuisineCustomers(new Cuisine(CuisineEnum.FRENCH));
        final List<Customer> italianCuisineCustomers = cuisinesRegistry.cuisineCustomers(new Cuisine(CuisineEnum.ITALIAN));

        Assert.assertEquals(1, italianCuisineCustomers.size());
        Assert.assertEquals(2, germanCuisineCustomers.size());
        Assert.assertEquals(3, frenchCuisineCustomers.size());

        Assert.assertEquals(italianCuisineCustomers.get(0).getUuid(), customer3Id);
        Assert.assertTrue(germanCuisineCustomers.stream().anyMatch(c->c.getUuid().equals(customer3Id)));
        Assert.assertTrue(germanCuisineCustomers.stream().anyMatch(c->c.getUuid().equals(customer2Id)));
    }

    @Test
    public void shouldGetEmptyCuisinesListForWrongUserId() {
        final List<Cuisine> customerCuisines = cuisinesRegistry.customerCuisines(new Customer(null));
        Assert.assertTrue(customerCuisines.isEmpty());
    }

    @Test
    public void shouldGetEmptyCustomersListForUknownCuisine() {
        final List<Customer> cuisineCustomers = cuisinesRegistry.cuisineCustomers(new Cuisine(null));
        Assert.assertTrue(cuisineCustomers.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptRegisteringNullUser() {
        cuisinesRegistry.register(null, new Cuisine(CuisineEnum.GERMAN));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptRegisteringNullUserId() {
        cuisinesRegistry.register(new Customer(null), new Cuisine(CuisineEnum.GERMAN));
    }

    @Test
    public void shouldNotAcceptRegisteringNullCuisineOrCuisineName() {
        Customer c = new Customer(UUID.randomUUID().toString());
        cuisinesRegistry.register(c, new Cuisine(null));
        Assert.assertEquals("Unknown cuisine, please reach johny@bookthattable.de to update the code\r\n", errContent.toString());
    }

    @Test
    public void shouldGetTheTopCuisines() {
        final List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(1);
        Assert.assertEquals(1, topCuisines.size());
        Assert.assertEquals(CuisineEnum.FRENCH, topCuisines.get(0).getName());
    }

    @Test
    public void shouldGetAllCuisinesOrderedInCaseOfHigherLimitThanListSize() {
        final List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(5);
        Assert.assertEquals(3, topCuisines.size());
    }

    @Test
    public void shouldGetEmptyTopCuisinesInCaseOfWrongLimit() {
        final List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(-15);
        Assert.assertTrue(topCuisines.isEmpty());
    }

    @Test
    public void shouldRegisterNewCuisine() {
        final Customer customer = new Customer(UUID.randomUUID().toString());
        final Cuisine cuisine = new Cuisine(CuisineEnum.SPANISH);
        cuisinesRegistry.register(customer, cuisine);
        final List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(customer);
        Assert.assertEquals(1, cuisines.size());
        Assert.assertEquals(cuisine.getName(), cuisines.get(0).getName());

        final List<Customer> customers = cuisinesRegistry.cuisineCustomers(cuisine);
        Assert.assertEquals(1, customers.size());
        Assert.assertEquals(customer.getUuid(), customers.get(0).getUuid());
    }
}