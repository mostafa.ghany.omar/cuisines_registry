package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private volatile Set<Cuisine> cuisines = Collections.newSetFromMap(new ConcurrentHashMap<>());

    @Override
    public void register(Customer customer, Cuisine cuisine) {
        if(isNull(cuisine) || isNull(cuisine.getName())){
            System.err.println("Unknown cuisine, please reach johny@bookthattable.de to update the code");
        } else if(isNull(customer) || isNull(customer.getUuid())){
            throw new IllegalArgumentException("Can't Register a null customer or null uuid");
        }

        boolean isKnownCuisine = false;
        for (Cuisine cuisineEl : cuisines) {
            if (cuisineEl.getName()==cuisine.getName()) {
                cuisineEl.getCustomers().add(customer);
                isKnownCuisine = true;
                break;
            }
        }

        if(!isKnownCuisine){
            cuisine.getCustomers().add(customer);
            cuisines.add(cuisine);
        }
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        if(isNull(cuisine) || isNull(cuisine.getName())){
            return new ArrayList<>();
        }

        final Set<Customer> customers = cuisines.stream()
                .filter(cuisine::equals)
                .map(Cuisine::getCustomers)
                .findFirst().orElse(new HashSet<>());

        return new ArrayList<>(customers);
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        if(isNull(customer) || isNull(customer.getUuid())){
            return new ArrayList<>();
        }

        return cuisines.stream()
                .filter(cuisine -> cuisine.getCustomers().contains(customer))
                .collect(Collectors.toList());
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        if(n<=0){
            return new ArrayList<>();
        }

        final ArrayList<Cuisine> topCuisines = new ArrayList<>(cuisines);
        topCuisines.sort(Comparator.comparing(c -> c.getCustomers().size(),
                            Comparator.reverseOrder()));
        if(topCuisines.size()>=n){
            return topCuisines.subList(0, n);
        }
        return topCuisines;
    }

}
