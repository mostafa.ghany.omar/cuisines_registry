package de.quandoo.recruitment.registry.model;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class Cuisine {

    private final CuisineEnum name;
    private volatile Set<Customer> customers = Collections.newSetFromMap(new ConcurrentHashMap<>());

    public Cuisine(final CuisineEnum name) {
        this.name = name;
    }

    public CuisineEnum getName() {
        return name;
    }

    public Set<Customer> getCustomers() {
        return customers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cuisine cuisine = (Cuisine) o;
        return name == cuisine.name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

}
