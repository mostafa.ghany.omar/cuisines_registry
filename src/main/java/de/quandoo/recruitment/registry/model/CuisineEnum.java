package de.quandoo.recruitment.registry.model;

public enum CuisineEnum {
    ITALIAN("italian"),
    FRENCH("french"),
    GERMAN("german"),
    SPANISH("spanish")//...
    ;

    private String value;

    CuisineEnum(String value){
        this.value = value;
    }

    public String getName() {
        return value;
    }

    public void setName(String value) {
        this.value = value;
    }
}
